import React, { Component } from "react";
import { TimePicker } from 'antd';
import 'antd/dist/antd.css';
import moment from 'moment';
import { Modal, Button } from 'antd';
import CustomApp from "./CustomApp";

class ModalArr extends Component{
  constructor(props){
    super(props);
    this.state = {
      isModalVisible: false,
      selectedTime: "00:00"
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.onChange = this.onChange.bind(this)

  }

  componentDidMount(){
    console.log(this.props.arrTime)
  }

  showModal(){
    this.setState({
      isModalVisible: true
    })
  }
  handleOk(){
    this.setState({
      isModalVisible: false,
    })
  }
  handleCancel(){
    this.setState({
      isModalVisible: false
    })
  }

  onChange(time, timeString){
    console.log(time, timeString)

    this.props.arrTime.push(timeString)
    console.log(this.props.arrTime)
  }
  render(){
    return (
      <div>
        <Button onClick={this.showModal} style={{backgroundColor: "indigo", color: "white"}}>
          Set Arrival Time
         </Button>
         <Modal title="Please set your arrival time" visible={this.state.isModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <TimePicker onChange={this.onChange} defaultValue={moment('00:00', 'HH:mm')} format="HH:mm"/>
          
        </Modal>
      </div>
    )
  }

}


export default ModalArr;