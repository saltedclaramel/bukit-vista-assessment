import React, { Component } from "react";
import en_US from "../../languageProvider/locales/en_US.json";
import Form from '../../components/uielements/form';
import ModalArr from './ModalArr'

import '../containers/CustomApp.css';

class CustomApp extends Component {
  constructor(props){
    super(props);
    this.state = {
      code: "",
      errors: {},
      externalLabel: null,
      time: [],
      timeArrLength: 0
    }
    this.handleChange = this.handleChange.bind(this);
    console.log(this.state.time.length)
    console.log(this.state.time)

  }
  handleChange = (e) => {
    let formIsValid = true;
    let errors = {};
    this.setState({
      code: e.target.value
    })
    if (typeof this.state.code !== "undefined"){
      if (!this.state.code.match(/^[A-Z0-9]+$/)){
        formIsValid = false
        errors["code"] = "Should be combination of numbers and alphabets"
      } 
    }
    this.setState({
      errors: errors
    })
  }

  componentDidMount(){

    this.setState({
      externalLabel: en_US["label.bookingcode"],
      sidebarMenu: en_US["customApp.sidebar"]

    })
  }
  
  render() {
    const { externalLabel, sidebarMenu } = this.state;
    let displayText;
    let timeLength = this.state.time.length
    if (timeLength !== 0){
      displayText = <span>{this.state.time[timeLength-1]} (Thank you, your host has been informed about your arrival.)</span>
    } else {
      displayText = <span>--:-- (please set your arrival time)</span>
    }
    return (
      <div>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"></link>
            <h1 style={{marginLeft: 40, paddingTop: 40}}>Bukit Vista</h1>
            <div className="row">
              <div className="col-3">
                <div className="sidebarMenu">
                  <h5 style={{color: "white"}}>{sidebarMenu}</h5>
                </div>
              </div>
              <div className="col-9">
                <div className="container">
                <center>
                  <Form style={{marginTop: 20}}>
                      <label>{externalLabel}</label>
                      <br/>
                      <input type="text" value={this.state.code} placeholder="KJSH87HGDK" onChange={ this.handleChange }/>
                      <br/>
                      <span style={{color: "red"}}>{this.state.errors["code"]}</span>
                  </Form>
                </center>
                <br/>
                <div className="table">
                  <tr>
                    <td>
                      Hi, John Doe!
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="4">
                      Thank you for booking with Bukit Vista! Here are the details of your current booking:
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Property name:
                    </td>
                    <td style={{fontWeight: "bold"}}>
                      Villa Bagus by Bukit Vista
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Check in date:
                    </td>
                    <td style={{fontWeight: "bold"}}>
                      3 April 2019
                    </td>
                    <td>
                      Check out date:
                    </td>
                    <td style={{fontWeight: "bold"}}>
                      5 April 2019
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Arrival time:
                    </td>
                    <td>
                      { displayText }
                    </td>
                  </tr>
                  <tr colSpan="4">
                    <ModalArr arrTime={this.state.time}/>
                  </tr>
                </div>

                </div>
              </div>
              
            </div>



        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
      </div>
    );
  }
}

export default CustomApp;